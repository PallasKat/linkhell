# README #

This is a minimal example that illustrate the compilation and the linkage of an OpenACC routine seq with an OpenACC program on a CSCS machine.

There are two main examples: (i) building a library written in Fortran and linking it with a driver written in Fortran. (ii) Building a library written in C/C++ and linking it with a driver written in Fortran. In all cases the example contains accelerated code (OpenAcc).

### What is this repository for? ###

* Proof of concept and test to understand the behavior of the NV-linker
* Version 0.2

### How do I get set up the Kesch/Escha example? ###

Log on on Kesch or Escha and clone the repository, then there are two ways to test the application. The first one is automated and illustrate a case that works and one that doesn't work. The other consists in applying all the steps (commands) described below.

Before any compilation or linkage, do not forget to load the following modules:
```
module purge
module load craype-haswell
module load craype-accel-nvidia35
module load PrgEnv-cray/15.10_cuda_7.0
module load cmake/3.1.3
module swap cce/8.4.0a
module load cray-libsci_acc/3.3.0
module load cray-netcdf/4.3.2
module load cray-hdf5/1.8.13
module load GCC/4.9.3-binutils-2.25
```
These modules are also required for the execution on Kesch or Escha.

## Compile and link Fortran with a Fortran library ##
The first scenario illustrate the case when a library written in Fortran is linked with a Fortran project. Both (the library and the driver) contains accelerated code with OpenAcc.

### Compile and link with the Cray compiler ###
The directory `fortran/grouped` is an example where the `makefile` allows to correctly build the application but it's not relevant to the physic-standalone (radiation). So to link and compile, go to the `fortran/grouped` directory and execute `make all`. This example compile and link correctly.

To execute it, do not forget to export the following variable: 
```
export G2G=1
```
Then you can execute the application with `srun`: 
```
srun -n 1 -p debug --gres=gpu:1 -t 00:05:00 ./hell.out
```
and should produce an output similar to this one:
```
[INFO] Init arrays x and y

[INFO] GPU execution

[INFO] Start parallel loop (1)
[INFO] Print results (2)
1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.,  10.
2.,  4.,  6.,  8.,  10.,  12.,  14.,  16.,  18.,  20.
3.,  6.,  9.,  12.,  15.,  18.,  21.,  24.,  27.,  30.
[INFO] Start parallel loop (2)
[INFO] Print results (2)
1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.,  10.
2.,  4.,  6.,  8.,  10.,  12.,  14.,  16.,  18.,  20.
3.,  6.,  9.,  12.,  15.,  18.,  21.,  24.,  27.,  30.

[INFO] CPU execution

[INFO] Start sequential loop (3)
[INFO] Print results (3)
1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.,  10.
2.,  4.,  6.,  8.,  10.,  12.,  14.,  16.,  18.,  20.
3.,  6.,  9.,  12.,  15.,  18.,  21.,  24.,  27.,  30.
[INFO] Start sequential loop (4)
[INFO] Print results (4)
1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9.,  10.
2.,  4.,  6.,  8.,  10.,  12.,  14.,  16.,  18.,  20.
3.,  6.,  9.,  12.,  15.,  18.,  21.,  24.,  27.,  30.
```

The directory `fortran/split` contains a `makefile` that mimics more the situation of the physic-standalone (radiation), but the latter cannot be linked by the Cuda linker (`nvlink`). So to test this degenerated case, go the `split` directory and execute `make all`. This example compile but doesn't link correctly:
```
ftn -h acc -L./install -lflib hell.o -o hell.out
nvlink error   : Undefined reference to 'addthem$flib_' in '/tmp/pe_56984//app_cubin_56984.o__hell.o__sec.cubin'
cuda_link: nvlink fatal error
make: *** [all] Error 1
```
But this will be detailed in the next section.

### Step that is failing with the Cray compiler (split directory) ###
These commands are executed in the `split` directory. When there is a need to change directory, the latter will be specified. If not one must assume that the command is executed in the `split` directory. First, do not forget to load the environment specified in the previous step.

* You can either copy past the command from the `Makefile` or execute `make clean`, `make copy`, `make lib` and `make hell`

*  Then you try to link the objects and the library: 
```
ftn -h acc -L./install/ -lflib hell.o -o hell.out
```
and this produces the following error:
```
nvlink error   : Undefined reference to 'addthem$flib_' in '/tmp/pe_56984//app_cubin_56984.o__hell.o__sec.cubin'
cuda_link: nvlink fatal error
make: *** [all] Error 1
```

### Step by step commands that are failing with the PGI compiler (split directory) ###


* Load the environment
```
module load PrgEnv-pgi/16.3
module load cmake/3.1.3
```

* Compile the seq routine in the `fortran/split/module` directory (if needed copy the file with `make copy`):
```
pgfortran -c flib.f90 -o flib.o -ta=tesla -Minfo
```
output:
```
addthem:
     13, Generating acc routine seq

```

* Create the library from the created object file in the `fortran/split/module` directory::
```
ar rvs libflib.a flib.o
```
output:
```
ar: creating libflib.a
a - flib.o
```

* Compile the main program: 
```
pgfortran -c hell.f90 -ta=tesla -Minfo -module module/
```
output:
```
hell:
     20, Accelerator kernel generated
         Generating Tesla code
         21, !$acc loop gang, vector(10) ! blockidx%x threadidx%x
     20, Generating copy(y(:),x(:))
         Generating copyout(z(:))
     32, Accelerator kernel generated
         Generating Tesla code
         33, !$acc loop gang, vector(10) ! blockidx%x threadidx%x
     32, Generating copy(y(:),x(:))
         Generating copyout(z(:))

```

* Link the objects and the library: 
```
pgfortran -L. -l/module/flib hell.o -o hell.out -ta=tesla -Minfo
```
producing the following error:
```
nvlink error   : Undefined reference to 'flib_addthem_' in 'hell.o'
pgacclnk: child process exit status 2: /appsmnt/escha/system/opt/pgi/linux86-64/16.3/bin/pgnvd
```

## Compile and link Fortran with a C/C++ library ##
The second scenario illustrate the case when a library written in C++ is linked with a Fortran project. Both (the library and the driver) contains accelerated code with OpenAcc. The root of this second example is in the `clib` directory. There are three scenarios: (i) no library is built, only the object file is used. (ii) The library is built and installed in the same directory as the Fortran sources. (iii) The library is built and installed in custom directories.

### How do I get set up the Daint example? ###
One should note that these example should be executed on the CSCS machine Daint, as it's the only one that propose the module CCE 8.4.1. Hence do not forget to swap the modules:
```
module swap cce/8.4.1
```
and the loaded modules should be:
```
  1) modules/3.2.10.3                       8) totalview/8.11.0                      15) xpmem/0.1-2.0502.64982.5.3.ari        22) slurm
  2) eswrap/1.1.0-1.020200.1231.0           9) cray-libsci/13.0.4                    16) dvs/2.5_0.9.0-1.0502.2188.1.116.ari   23) cray-mpich/7.2.2
  3) switch/1.0-1.0502.60522.1.61.ari      10) udreg/2.3.2-1.0502.10518.2.17.ari     17) alps/5.2.4-2.0502.9822.32.1.ari       24) ddt/6.0
  4) craype-network-aries                  11) ugni/6.0-1.0502.10863.8.29.ari        18) rca/1.0.0-2.0502.60530.1.62.ari       25) cudatoolkit/6.5.14-1.0502.9836.8.1
  5) cce/8.4.1                             12) pmi/5.0.7-1.0000.10678.155.25.ari     19) atp/1.8.2                             26) cray-libsci_acc/3.1.1
  6) craype/2.4.0                          13) dmapp/7.0.1-1.0502.11080.8.76.ari     20) PrgEnv-cray/5.2.82                    27) craype-accel-nvidia35
  7) totalview-support/1.1.4               14) gni-headers/4.0-1.0502.10859.7.8.ari  21) craype-sandybridge
```

### Using only object files ###
To test this scenario go to the `clib/grouped` directory and execute `make all`:
```
> make all
rm -f *.h *.cpp *.cub *.ptx *.f90 *.mod *.o *.out *.a
cp ../src/inferno.f90 ./
cp ../src/f2c.f90 ./
cp ../src/mycos.h ./
cp ../src/mycos.cpp ./	
CC -h acc -c mycos.cpp 
ftn -h acc -c f2c.f90
ftn -h acc -c inferno.f90
ftn -h acc mycos.o f2c.o inferno.o -o inferno.out
> 
```
This case is correctly compiled and linked.

### Creating a library in the same location as the sources ###
To test this scenario go to the `clib/libgrouped` directory and execute `make all`:
```
> make all
rm -f *.h *.cpp *.cub *.ptx *.f90 *.mod *.o *.out *.a
cp ../src/inferno.f90 ./
cp ../src/f2c.f90 ./
cp ../src/mycos.h ./
cp ../src/mycos.cpp ./	
CC -h acc -c mycos.cpp
ar rvs libcccos.a mycos.o
ar: creating libcccos.a
a - mycos.o
ftn -h acc -c f2c.f90
ftn -h acc -c inferno.f90
ftn -h acc -L. -lcccos f2c.o inferno.o -o inferno.out
```
And this produce the following link error:
```
nvlink error   : Undefined reference to 'br_cos' in '/tmp/pe_30291//app_cubin_30291.o__inferno.o__sec.cubin'
cuda_link: nvlink fatal error
```
However a simple check shows that the symbol exists in the library:
```
> ar x libcccos.a 
> nm mycos.o 
0000000000000000 T br_cos
                 U $cray$_global_cuda_module_cache
                 U $cray$_global_cuda_module_data
                 U $cray$_global_cuda_module_data_size
0000000000000000 r .LCPI0_0
0000000000000008 r .LCPI0_1
0000000000000010 r .LCPI0_2
>
```

### Creating and installing a library in specific locations ###
This case produces the same error as the previous one. One can try:
```
> make all
rm -f *.h *.cpp *.cub *.ptx *.f90 *.mod *.o *.out *.a
rm -rf ./module ./install
mkdir -p module
mkdir -p install/lib
mkdir -p install/include
cp ../src/inferno.f90 ./
cp ../src/f2c.f90 ./
cp ../src/mycos.h ./module/
cp ../src/mycos.cpp ./module/
CC -h acc -c module/mycos.cpp -o module/mycos.o
ar rvs module/libcccos.a module/mycos.o
ar: creating module/libcccos.a
a - module/mycos.o
mv ./module/libcccos.a ./install/lib/
mv ./module/mycos.h ./install/include/
ftn -h acc -c f2c.f90
ftn -h acc -c inferno.f90
ftn -h acc -L./install/lib -lcccos f2c.o inferno.o -o inferno.out
nvlink error   : Undefined reference to 'br_cos' in '/tmp/pe_3030//app_cubin_3030.o__inferno.o__sec.cubin'
cuda_link: nvlink fatal error
make: *** [all] Error 1
```

## Contribution guidelines ##

* Code written by Christophe Charpilloz

## Who do I talk to? ##

* Repo owner or admin