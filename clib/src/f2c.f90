module f2c
  use iso_fortran_env
  implicit none
  
  interface
    pure real(c_double) function CFCT(x) result(y) bind(C, name="br_cos")
      !$acc routine seq
      use iso_c_binding
      implicit none
      real(c_double), value :: x
    end function CFCT
  end interface

end module f2c
