program inferno
  use iso_fortran_env
  use f2c
  implicit none

  integer(kind=int32), parameter :: N = 10
  integer(kind=int32) :: i
  real(kind=real64), dimension(N) :: x, y

  print *, "[INFO] Init arrays x"
  do i=1,N
    x(i) = real(i)
  end do

  print *, "============================="
  print *, "[INFO] GPU execution"
  print *, "============================="

  print *, "[INFO] Start parallel loop (1)"
  !$acc parallel loop
  do i=1,N
    y(i) = CFCT(x(i))
  end do
  !$acc end parallel loop

  print *, "[INFO] Print results (1)"
  print *, x
  print *, y

  print *, "============================="
  print *, "[INFO] CPU execution"
  print *, "============================="

  print *, "[INFO] Start sequential loop (2)"
  do i=1,N
    y(i) = CFCT(x(i))
  end do

  print *, "[INFO] Print results (2)"
  print *, x
  print *, y

end program inferno
