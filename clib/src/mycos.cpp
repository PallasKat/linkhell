#include "mycos.h"

#pragma acc routine seq
double br_cos(double x) {
  return 1.0 + x*x/2.0 - x*x*x*x/24.0;
}

