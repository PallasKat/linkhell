module flib
  use iso_fortran_env
  implicit none

  interface FCT
    module procedure addthem
  end interface FCT

contains
  pure real(kind=real64) function addthem(x, y) result(z)
    !$acc routine seq
    real(kind=real64), intent(in) :: x, y
    z = x + y
  end function addthem

end module flib
