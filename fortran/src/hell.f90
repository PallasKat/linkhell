program hell
  use iso_fortran_env
  use flib
  implicit none

  integer(kind=int32) :: i
  real(kind=real64), dimension(10) :: x, y, z

  print *, "[INFO] Init arrays x and y"
  do i=1,10
    x(i) = real(i)
    y(i) = 2.0*i
  end do

  print *, "============================="
  print *, "[INFO] GPU execution"
  print *, "============================="

  print *, "[INFO] Start parallel loop (1)"
  !$acc parallel loop
  do i=1,10
    z(i) = addthem(x(i), y(i))
  end do
  !$acc end parallel loop

  print *, "[INFO] Print results (2)"
  print *, x
  print *, y
  print *, z

  print *, "[INFO] Start parallel loop (2)"
  !$acc parallel loop
  do i=1,10
    z(i) = FCT(x(i), y(i))
  end do
  !$acc end parallel loop

  print *, "[INFO] Print results (2)"
  print *, x
  print *, y
  print *, z

  print *, "============================="
  print *, "[INFO] CPU execution"
  print *, "============================="

  print *, "[INFO] Start parallel loop (3)"
  do i=1,10
    z(i) = addthem(x(i), y(i))
  end do

  print *, "[INFO] Print results (3)"
  print *, x
  print *, y
  print *, z

  print *, "[INFO] Start parallel loop (4)"
  do i=1,10
    z(i) = FCT(x(i), y(i))
  end do

  print *, "[INFO] Print results (4)"
  print *, x
  print *, y
  print *, z

end program hell
